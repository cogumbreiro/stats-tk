```
$ stk-sample -h
usage: stk-sample [-h] [-o OUTPUT] INPUT

Compute stats of a sample.

positional arguments:
  INPUT       The samples should be a sequence of numbers (a number per line).
              Use '-' for stdin.

optional arguments:
  -h, --help  show this help message and exit
  -o OUTPUT   A JSON file with stats about the samples. Default: stdout
```

The program `stk-sample` computes some stats from a sample.
The input should be a number per line and it is treated as a floating
point number.

For example:
```
#!bash
$ seq 10 | ./stk-sample -
{"std": 2.8722813232690143, "min": 1.0, "max": 10.0, "hash_func": "md5", "size": 10, "hash_digest": "3b0332e02daabf31651a5a0d81ba830a", "mean": 5.5}
```

The program `stk-confidence` computes the confidence interval for 
a given sample input. It requires the mean, the sample size, and the
standard deviation `std`. The program just appends information, so
all keys in the input are retained.

```
stk-confidence -h
usage: stk-confidence [-h] [-o OUTPUT | -i] [-c coeficient] INPUT

Compute the margin error for a given confidence coeficient.

positional arguments:
  INPUT          A JSON file with stats about the samples. Use '-' for stdin.

optional arguments:
  -h, --help     show this help message and exit
  -o OUTPUT      A JSON file with stats about the samples. Default: stdout.
  -i             Writes file in place.
  -c coeficient  The confidence coeficient (between 0 and 1).
```

For example

```
#!bash
$ seq 10 | stk-sample - | stk-confidence -
{"std": 2.8722813232690143, "margin_error": 2.0547062804390821, "min": 1.0, "max": 10.0, "hash_func": "md5", "confidence_coeficient": 0.95, "mean": 5.5, "hash_digest": "3b0332e02daabf31651a5a0d81ba830a", "size": 10}
```


